# DAO and Charity
Problem Statement
In Charity based organizations, multiple charity projects are proposed by different team members. Depending upon the need and importance of the project, team members prioritize the proposals by priority voting system. Funds are then released to the relevant proposal acccordingly

## App Flow
## Register
To register for a specific charity group, interested person needs to register with that group by paying some stacks and buying DAO tokens. These tokens will then be used for voting for a proposal and submitting a proposal.

## AddFunds
This method is called by the FundsManager. He'll be calling this function whenever there is some amount to be added in the funds bucket.
In real world, charity organizations receive funds from different other organizations, governement and donors. This method will keep the record of all new charities being received.


## SubmitProposal
Whenever some member from organization wants to propose a project, he submits the proposal. Proposal contains some description and the amount needed for the project.

## BroadcastProposal
Depending upon the funds in the funds bucket, DAO contract will broadcast the proposals in queue. It'll be done on FIFO basis. In case of handful amount of funds, more than one proposals can be broadcasted. Proposals will be broadcasted for 20 blocks.

## Submit Vote
Team members can vote for the broadcasted proposals. 
The proposal will be accepted on votes by atleast 60% team members, else the proposal would be considered rejected.
If there're more than one proposals in queue, voting will be done with defining a priority number for the proposal on which vote is being casted.

## Reject Proposal
This will be called by SubmitVote in case some project is rejected by insufficient number of votes

## ReleaseFunds
This method will be called by DAO contract whenever some amount needs to be released to a project's proposal. This will be done on the basis of priority queue

