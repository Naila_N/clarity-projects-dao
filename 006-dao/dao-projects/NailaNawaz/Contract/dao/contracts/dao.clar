
;; constants
;;
(define-constant REGISTERATION_COST u20)
(define-constant PROPOSAL_FEE u10)
(define-constant ERR_ALREADY_REGISTERED (err u101))
(define-constant NOT_A_MEMBER (err u102))
(define-constant NOT_ENOUGH_DAO_TOKENS (err u103))
(define-data-var funds-bucket uint u0)
(define-data-var funds-available uint u0)
(define-fungible-token dao-tokens u2000000)
(define-data-var proposal-count uint u0)
(define-data-var last-processed-proposal uint u0)

;; data maps and vars
;;this map will create a track of all the members 
(define-map member-token {member: principal} {membership: bool})
(define-map proposals {proposal-id: uint} 
        {proposed-by: principal,
        required-amount: uint,
        draft-url: (string-ascii 100)})
;; this map will keep a track of proposal status of all the proposals
;; 0: not processed, 1: under process, 2: processed
(define-map proposal-status {proposal-id: uint} {status: uint})
;; this map will keep a track of acceptance state of processed proposals
;; true for accepted and false for rejected
(define-map proposal-acceptance-status uint bool)
;; this map will keep track of proposal ids and the block height at which the voting was started
(define-map vote-record {proposal-id: uint} {block-height: uint})
;; private functions

(define-private (is-member (stx-address principal))
    (ok (is-some (map-get? member-token {member: stx-address})))
)

(define-private (tranfer-dao-tokens (amount uint) (address principal))
    (ft-mint? dao-tokens amount address)
)

;; this method will be called by contract whenever there are new funds in the bucket
(define-private (process-pending-proposals)
    (let
        (
            (proposal-to-process ( + (var-get last-processed-proposal) u1))
            (amount-needed (default-to u0 (get required-amount (map-get? proposals {proposal-id: proposal-to-process}))))
        )
        (if (>= amount-needed (var-get funds-available ))
            (begin
                ;;start voting
                (map-set proposal-status {proposal-id: proposal-to-process} {status: u1})
                ;; record block height
                (map-set vote-record {proposal-id: proposal-to-process} {block-height: block-height})

                (ok true)
            )
            (ok false)
        )
    )
)

;; (define-public (vote (proposal-to-process uint))
;; )

(define-private (destroy (amount uint) (address principal))
    (ft-burn? dao-tokens amount address)
)

;; this function can be called by anyone to get the membership with this charity group. 
;; For the first time, member will have to transfer atleast 20 stacks to mint 20 dao-tokens and get the membership
(define-public (register-member)
    (begin 
        ;; Check if the caller is not already a member
        (asserts! (is-eq (unwrap-panic (is-member tx-sender)) false) ERR_ALREADY_REGISTERED)
        ;; if the caller is not already a member, then transfer required stx from caller to contract
        (try! (stx-transfer? REGISTERATION_COST tx-sender (as-contract tx-sender)))
        ;; keep a track of stx in funds bucket
        (var-set funds-bucket (+ (var-get funds-bucket) REGISTERATION_COST))
        (var-set funds-available (+ (var-get funds-available) REGISTERATION_COST))
        ;;transfer 20 dao tokens to the new member
        (try! (tranfer-dao-tokens REGISTERATION_COST tx-sender) )
        ;; insert the member in mapping along with the assigned tokens
        (map-insert member-token {member: tx-sender} {membership: true})
        ;; process any pending proposals in queue
        (process-pending-proposals) 
    )
)

;; this function will be called by the registered members only for submitting a proposal
(define-public (submit-proposal (amount uint) (draft-url (string-ascii 100)))
    (let
        (
            (sender-token-balance (unwrap-panic (get-balance-of tx-sender)))
            (new-proposal-id (+ (var-get proposal-count) u1))
        )
        ;; check if the sender is member of this charity group
        (asserts! (is-eq (unwrap-panic (is-member tx-sender)) true) NOT_A_MEMBER)
        ;; check if the user has 2 dao tokens for submitting proposal
        (asserts! (>= sender-token-balance PROPOSAL_FEE) NOT_ENOUGH_DAO_TOKENS)
        ;; charge 2 dao tokens from sender
        (try! (destroy PROPOSAL_FEE tx-sender))
        ;; add the proposal in proposals mapping
        (map-insert proposals {proposal-id: new-proposal-id} 
                    {proposed-by: tx-sender,
                    required-amount: amount,
                    draft-url: draft-url})
        (map-insert proposal-status {proposal-id: new-proposal-id} {status: u0})
        ;; increment proposals count
        (var-set proposal-count new-proposal-id)
        ;;process-proposal
        (ok true)
    )
)
;; this can be called by registered members to buy dao tokens for submitting proposals 1stx = 1 dao token
(define-public (buy-dao-tokens (amount uint))
    (begin
        ;; check if the sender is member of this charity group
        (asserts! (is-eq (unwrap-panic (is-member tx-sender)) true) NOT_A_MEMBER)
        ;; charge the required stx
        (try! (stx-transfer? amount tx-sender (as-contract tx-sender)))
        ;; keep a track of stx in funds bucket
        (var-set funds-bucket (+ (var-get funds-bucket) amount))
        ;;transfer 20 dao tokens to the new member
        (try! (tranfer-dao-tokens amount tx-sender) )
        (ok true)
    )
)

;; any member/non-member can call this method to donate some stacks to this charity group
;; dao-tokens (required for submitting proposals) will not be issued by this method
(define-public (donate-stx (amount uint))
    (begin
        ;; charge the required stx
        (try! (stx-transfer? amount tx-sender (as-contract tx-sender)))
        ;; keep a track of stx in funds bucket
        (var-set funds-bucket (+ (var-get funds-bucket) amount))   
        (var-set funds-available (+ (var-get funds-available) amount))
        ;; process any pending proposals in queue
        (process-pending-proposals) 
    )
)

(define-read-only (get-balance-of (account principal))
    (ok (ft-get-balance dao-tokens account) )
)